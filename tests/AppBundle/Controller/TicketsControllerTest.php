<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class TicketsControllerTest extends WebTestCase
{
    // Test sur l'action informationsAction du controller Tickets (Réponse 200 + submit du form)

    public function testInformations()
    {
        date_default_timezone_set("Europe/Paris");
        $client = static::createClient();

        $crawler = $client->request('GET', '/fr/informations');
        $this->assertEquals('AppBundle\Controller\TicketsController::informationsAction', $client->getRequest()->attributes->get('_controller'));

        $response = $client->getResponse();

        $form = $crawler->selectButton('appbundle_commande[save]')->form();
        // Form avec mauvaises valeurs
        $form['appbundle_commande[dateVisite]'] = '20/12/2016';
        $form['appbundle_commande[nbBillets]'] = '0';
        $form['appbundle_commande[email]'] = 'email@freee.Fr';

        $crawler = $client->submit($form);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('span.help-block')->count());
        // Form avec bonnes valeurs
        $form['appbundle_commande[dateVisite]'] = '20/02/2017';
        $form['appbundle_commande[nbBillets]'] = '4';
        $form['appbundle_commande[email]'] = 'email@free.fr';

        $client->submit($form);
        $this->assertEquals(200, $response->getStatusCode());
        $client->followRedirect();
        $this->assertEquals('AppBundle\Controller\TicketsController::reservationAction', $client->getRequest()->attributes->get('_controller'));

    }

    // Test sur quelques pages du site
    /**
     * @dataProvider urlProvider
     * @param $url
     */
    public function testPageIsSuccessful($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider()
    {
        return array(
            array('/fr/informations'),
            array('/en/'),

        );
    }

}
