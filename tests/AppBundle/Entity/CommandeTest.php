<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 26/01/2017
 * Time: 09:23
 */
namespace Tests\Entity;

use AppBundle\Entity\Commande;


class CommandeTest extends \PHPUnit_Framework_TestCase
{
    public function testCommande()
    {
        date_default_timezone_set("Europe/Paris");

        $commande = new Commande();

        $commande
            ->setDateVisite('27/02/2017')
            ->setTypeBillet(true)
            ->setNbBillets('4')
            ->setEmail('test@free.Fr')
            ->setCodeReservation('TEST35551');


        $this->assertEquals('27/02/2017', $commande->getDateVisite());
        $this->assertEquals(true, $commande->getTypeBillet());
        $this->assertEquals('4', $commande->getNbBillets());
        $this->assertEquals('test@free.Fr', $commande->getEmail());
         $this->assertEquals('TEST35551', $commande->getCodeReservation());


    }
}