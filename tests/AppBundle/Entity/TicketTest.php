<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 25/01/2017
 * Time: 21:13
 */

namespace Tests\Entity;

use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;
use AppBundle\Services\Tarifs;

class BilletTest extends \PHPUnit_Framework_TestCase
{
    public function testBillet()
    {
        date_default_timezone_set("Europe/Paris");
        $billet = new Ticket();
        $billet1 = new Commande();
        $billet
            ->setTypeTarif('Tarif Senior')
            ->setPrix('12');
        $billet1
            ->setCodeReservation('TEST35551');

        $this->assertEquals('Tarif Senior', $billet->getTypeTarif());
        $this->assertEquals('12', $billet->getPrix());
        $this->assertEquals('TEST35551', $billet1->getCodeReservation());
    }
}