<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 26/01/2017
 * Time: 09:25
 */

namespace Tests\Entity;
use AppBundle\Entity\Ticket;

class VisiteurTest extends \PHPUnit_Framework_TestCase
{
    public function testVisiteur()
    {
        date_default_timezone_set("Europe/Paris");

        $dateTime = new \DateTime();
        $visiteur = new Ticket();
        $visiteur
            ->setNom('TEST')
            ->setPrenom('PRENOM')
            ->setPays('France')
            ->setdatenaissance($dateTime)
            ->settarifReduit(true);
        $this->assertEquals('TEST', $visiteur->getNom());
        $this->assertEquals('PRENOM', $visiteur->getPrenom());
        $this->assertEquals('France', $visiteur->getPays());
        $this->assertEquals($dateTime, $visiteur->getDateNaissance());
        $this->assertEquals(true, $visiteur->getTarifReduit());
    }
}