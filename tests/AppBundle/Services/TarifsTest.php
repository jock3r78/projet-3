<?php


namespace tests\AppBundle\Services;


use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;
use AppBundle\Services\Tarifs;

class TarifsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider ticketProvider
     */
    public function testCalculTarifTicket($date, $typeBillet, $tarifReduit, $resultat)
    {
        date_default_timezone_set("Europe/Paris");
        $tarifs = new Tarifs();
        $ticket = new Ticket();
        $commande = new Commande();
        $ticket->setdatenaissance(new \DateTime($date));
        $commande->setTypeBillet($typeBillet); //false correspondant à demi journée
        $ticket->setTarifReduit($tarifReduit);
        $commande->setDateReservation(new \DateTime('2017-01-02'));
        $ticket->setCommande($commande);
        $tarifs->calculTarifTicket($ticket);

        $this->assertEquals($resultat, $ticket->getPrix());
    }

    public function ticketProvider()
    {
        return array(
            array('1975-12-10', true, true, 10), // Adulte + Journée entière + Tarif réduit
            array('2010-11-03', false, false, 8), // Enfant + Demi journée + Tarif plein
            array('1930-08-21', true, false, 12) // Senior + Journée Entière + Tarif Plein
        );
    }

    public function testCalculTotal()
    {
        $tarifs = new Tarifs();
        $commande = new Commande();
        $ticket1 = new Ticket();
        $ticket1->setdatenaissance(new \DateTime('1975-12-10')); // Adulte
        $commande->setTypeBillet(true); //true correspondant à journée entière
        $ticket1->setTarifReduit(false);
        $commande->setDateReservation(new \DateTime('2017-01-02')); //On définit à la date du jour arbitrairement
        $ticket2 = new Ticket();
        $ticket2->setdatenaissance(new \DateTime('1996-12-10')); // Enfant
        $commande->setTypeBillet(false); //false correspondant à demi journée
        $ticket2->setTarifReduit(true);
        $ticket1->setCommande($commande); // On définit le ticket 1 dans la commande
        $ticket2->setCommande($commande); // ...puis le ticket 2
        $tarifs->calculTarifTicket($ticket1); // Calcul du prix du ticket 1
        $ticket1->getPrix(); // Récupération de son prix
        $tarifs->calculTarifTicket($ticket2);
        $ticket2->getPrix();
        $commande->addTicket($ticket1); // Ajout du ticket 1  à la commande
        $commande->addTicket($ticket2); // ...puis du ticket 2
        $tarifs->calculTotal($commande); // On appelle la méthode CalculTotal du service

        $this->assertEquals(26, $commande->getPrixTotal());

    }
}