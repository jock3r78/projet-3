<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commande;
use AppBundle\Form\CommandeType;
use AppBundle\Form\ResaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TicketsController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function informationsAction(Request $request)
    {
        $commande = new Commande();

        $form = $this->get('form.factory')->create(ResaType::class, $commande);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush();

            $this->addFlash('notice', 'Date et Billets Validés');

            return $this->redirectToRoute('louvre_reservation', array('codeReservation' => $commande->getCodeReservation()));
        }

        return $this->render('AppBundle:Tickets:informations.html.twig', array(
            'resaForm' => $form->createView(),
            'codeReservation' => $commande->getCodeReservation(),
            'dateVisite' => $commande->getDateVisite()
        ));
    }

    /**
     * @param Request $request
     * @param Commande $commande
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reservationAction(Request $request, Commande $commande)
    {
        if (!$commande->getTickets()->count()) {
            $commande->initTickets();
        }

        $form = $this->get('form.factory')->create(CommandeType::class, $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($commande);
            $em->flush();

            $this->addFlash('notice', 'Coordonnées Enregistrées avec succès');
            return $this->redirectToRoute('louvre_paiement',
                array(
                    'codeReservation' => $commande->getCodeReservation()
                )
            );
        }
        return $this->render('AppBundle:Tickets:reservation.html.twig', array(
            'form' => $form->createView(),
            'commande' => $commande
        ));
    }

    /**
     * @param Request $request
     * @param Commande $commande
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paiementAction(Request $request, Commande $commande)
    {

        $em = $this->getDoctrine()->getManager();

        $this->get('louvre.tarifs')->calculTotal($commande); //Appel de la méthode du service Tarifs de calcul du total

        if ($request->isMethod('POST')) {
            if ($this->get('louvre.stripe')->checkoutAction($request, $commande)) {
                $this->get('louvre.sendmail')->sendMail($commande);
                $commande->setPaymentStatus(Commande::STATUS_PAID);
                return $this->redirectToRoute('louvre_remerciements', array(
                    'codeReservation' => $commande->getCodeReservation()
                ));
            }
        }
        $em->flush();
        return $this->render('AppBundle:Tickets:paiement.html.twig', array(
            'commande' => $commande,
        ));
    }

    /**
     * @param Commande $commande
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function remerciementsAction(Commande $commande)
    {

        return $this->render('AppBundle:Tickets:remerciements.html.twig', array(
            'commande' => $commande
        ));
    }

}
