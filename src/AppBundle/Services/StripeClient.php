<?php


namespace AppBundle\Services;

use AppBundle\Entity\Commande;
use Stripe\Stripe;
use Stripe\Charge;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class StripeClient
{
    /**
     * StripeClient constructor.
     * @param Session $session
     * @param $stripeSecretKey
     */
    public function __construct(Session $session, $stripeSecretKey)
    {
        $this->session = $session;
        Stripe::setApiKey($stripeSecretKey);
    }

    /**
     * @param Request $request
     * @param Commande $commande
     * @return bool
     */
    public function checkoutAction(Request $request, Commande $commande)
    {
        // Get the credit card details submitted by the form
        $token = $request->request->get('stripeToken');

        // Create a charge: this will charge the user's card
        try {
            Charge::create(array(
                "amount" => $commande->getPrixTotal() * 100,
                "currency" => "eur",
                "source" => $token,
                "description" => "Commande N°" . $commande->getCodeReservation()
            ));
            return true;

        } catch (\Exception $e) {
            $this->session->getFlashBag()->add('error', 'Erreur de paiement, veuillez vérifier vos coordonnées, les billets d entree à 0 ne sont pas possible sans billets payant');
            return false;
        }
    }
}