<?php

namespace AppBundle\Services;

use AppBundle\Entity\Ticket;
use AppBundle\Entity\Commande;

class Tarifs
{

    const TARIFBEBE = 0;
    const TARIFENFANT = 8;
    const TARIFNORMAL = 16;
    const TARIFSENIOR = 12;
    const TARIFREDUIT = 10;

    /**
     * @param Ticket $ticket
     * @return Ticket
     */
    public function calculTarifTicket(Ticket $ticket)
    {
        $commande= $ticket->getCommande();
        $dob = $ticket->getdatenaissance();
        $date_visite = $commande->getDateReservation();
        $age = $date_visite->diff($dob)->y;

        if ($commande->getTypeBillet() === "false") {
            $impact = 0.5; // Tarif demi-journée
        } else {
            $impact = 1;
        }
        switch ($ticket) {

            case ($age >= 4 && $age <= 12):
                $ticket->setPrix(self::TARIFENFANT * $impact);
                $ticket->setTypeTarif('Tarif Enfant');
                break;

            case ($age > 12 && $age < 60):
                $ticket->setPrix(self::TARIFNORMAL * $impact);
                $ticket->setTypeTarif('Tarif Normal');
                break;

            case ($age > 60):
                $ticket->setPrix(self::TARIFSENIOR * $impact);
                $ticket->setTypeTarif('Tarif Senior');
                break;

            case ($age < 4):
                $ticket->setPrix(self::TARIFBEBE * $impact);
                $ticket->setTypeTarif('Tarif Bébé');
                break;
        }
        if ($ticket->getTarifReduit() === true && $ticket->getPrix() >= self::TARIFREDUIT * $impact) { //Cas du tarif réduit
            $ticket->setPrix(self::TARIFREDUIT * $impact);
            $ticket->setTypeTarif('Tarif Réduit');
        }
        return $ticket;
    }

    /**
     * @param Commande $commande
     */
    public function calculTotal(Commande $commande)
    {

        $prixTotal = 0;
        foreach ($commande->getTickets() as $ticket) { // On boucle pour récupérer le tarif de chaque billet
            $this->calculTarifTicket($ticket);
            $prixTotal += $ticket->getPrix();
        }
        $commande->setPrixTotal($prixTotal);
    }

    /**
     * @return array
     */
    public function getPrix()
    {
        return array(
            array(
                'nom' => 'price.nom1',
                'prix' => self::TARIFBEBE,
                'info_1' => 'price.info_11',
                'info_2' => 'price.info_12'
            ),
            array(
                'nom' => 'price.nom2',
                'prix' => self::TARIFENFANT,
                'info_1' => 'price.info_21',
                'info_2' => 'price.info_22'
            ),
            array(
                'nom' => 'price.nom3',
                'prix' => self::TARIFNORMAL,
                'info_1' => 'price.info_31',
                'info_2' => 'price.info_32'
            ),
            array(
                'nom' => 'price.nom4',
                'prix' => self::TARIFSENIOR,
                'info_1' => 'price.info_41',
                'info_2' => 'price.info_42'
            ),
            array(
                'nom' => 'price.nom5',
                'prix' => self::TARIFREDUIT,
                'info_1' => 'price.info_51',
                'info_2' => 'price.info_52'
            )
        );
    }
}