<?php


namespace AppBundle\Services;

use AppBundle\Entity\Commande;
use Symfony\Component\Templating\EngineInterface;


class SendMail
{
    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $mailsender)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->mailsender = $mailsender;
    }

    /**
     * @param Commande $commande
     * @return bool
     */
    public function sendMail(Commande $commande)
    {
        try{

            $message = \Swift_Message::newInstance()
                ->setSubject('Confirmation de votre commande')
                ->setFrom($this->mailsender)
                ->setTo($commande->getEmail())
                ->setBody(
                    $this->templating->render(
                        'AppBundle:Emails:confirmationCommande.html.twig',
                        array('commande' => $commande)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
            return true;
        }
        catch (\Swift_TransportException $e)
        {
            return false;
        }
    }
}