<?php

namespace AppBundle\Entity;

use AppBundle\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as FormConstraint;
use AppBundle\Validator\RestrictedTime;

/**
 * Commande
 *
 * @ORM\Table(name="commande")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommandeRepository")
 */
class Commande
{
    const STATUS_AWAITING = 'En attente';
    const STATUS_PAID = 'Succès';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_visite", type="date",nullable=false)
     * @Assert\DateTime()
     * @Assert\NotBlank(message="Merci de choisir une date ")
     * @AppAssert\LimiteBillets()
     * @AppAssert\RestrictedDates()
     * @Assert\GreaterThanOrEqual(
     *      "today",
     *      message = "merci ne pas choisir une date antérieure à celle du jour.."
     *
     * )
     */
    private $dateVisite;

    /**
     * @var \DateTime
     * @ORM\Column(name="date_reservation", type="date", nullable=true)
     *
     */
    private $dateReservation;

    /**
     * @var string
     *
     * @ORM\Column(name="code_reservation", type="string", nullable=true)
     */
    private $codeReservation;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_billets", type="integer")
     * @Assert\NotNull( message="Vous devez indiquer au moins Un Billet (Limite à 10 billets)")
     * @Assert\Range(min="1", max="10",
     *      maxMessage="Vous ne pouvez pas réservez plus de 10 billets à la fois",
     *      minMessage="Vous devez commander au moins un billet"
     * )
     */
    private $nbBillets;

    /**
     * @var string
     *
     * @ORM\Column(name="type_billet", type="string", length=255)
     * @RestrictedTime()
     */
    private $typeBillet;

    /**
     * @var float
     *
     * @ORM\Column(name="prix_total", type="float", nullable=true)
     */
    private $prixTotal;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="merci de saisir un email")
     * @Assert\Email(checkMX=true, message="Aucun serveur mail n'a été trouvé pour ce domaine")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_status", type="string", length=255, nullable=true)
     */
    private $paymentStatus;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Ticket", mappedBy="commande", cascade={"persist"})
     * @Assert\Valid()
     */
    private $tickets;

    public function __construct()
    {
        $this->dateVisite = new \Datetime("now", new \DateTimeZone('Europe/Paris'));
        $this->tickets = new ArrayCollection();
        $this->setCodeReservation((str_split(str_shuffle("ABCDEFGHI"), 4)[0]) . random_int(1,99999));
        $this->dateReservation = new \DateTime();
        $this->paymentStatus = self::STATUS_AWAITING;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateVisite
     *
     * @param \DateTime $dateVisite
     *
     * @return Commande
     */
    public function setDateVisite($dateVisite)
    {
        $this->dateVisite = $dateVisite;

        return $this;
    }

    /**
     * Get dateVisite
     *
     * @return \DateTime
     */
    public function getDateVisite()
    {
        return $this->dateVisite;
    }

    /**
     * Set dateReservation
     *
     * @param \DateTime $dateReservation
     *
     * @return Commande
     */
    public function setDateReservation($dateReservation)
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    /**
     * Get dateReservation
     *
     * @return \DateTime
     */
    public function getDateReservation()
    {
        return $this->dateReservation;
    }

    /**
     * Set codeReservation
     *
     * @param string $codeReservation
     *
     * @return Commande
     */
    public function setCodeReservation($codeReservation)
    {
        $this->codeReservation = $codeReservation;

        return $this;
    }

    /**
     * Get codeReservation
     *
     * @return string
     */
    public function getCodeReservation()
    {
        return $this->codeReservation;
    }

    /**
     * Set nbBillets
     *
     * @param integer $nbBillets
     *
     * @return Commande
     */
    public function setNbBillets($nbBillets)
    {
        $this->nbBillets = $nbBillets;

        return $this;
    }

    /**
     * Get nbBillets
     *
     * @return int
     */
    public function getNbBillets()
    {
        return $this->nbBillets;
    }

    /**
     * Set prixTotal
     *
     * @param float $prixTotal
     *
     * @return Commande
     */
    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }

    /**
     * Get prixTotal
     *
     * @return float
     */
    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Commande
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set paymentStatus
     *
     * @param string $paymentStatus
     *
     * @return Commande
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Add ticket
     *
     * @param \AppBundle\Entity\Ticket $ticket
     *
     * @return Commande
     */
    public function addTicket(\AppBundle\Entity\Ticket $ticket)
    {
        $this->tickets[] = $ticket;
        $ticket->setCommande($this);

        return $this;
    }

    /**
     * Remove ticket
     *
     * @param \AppBundle\Entity\Ticket $ticket
     */
    public function removeTicket(\AppBundle\Entity\Ticket $ticket)
    {
        $this->tickets->removeElement($ticket);
    }

    /**
     * Get tickets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    public function initTickets()
    {
        for($i=0; $i<$this->nbBillets; $i++)
        {
            $this->addTicket(new Ticket());
        }
    }

    /**
     * Set typeBillet
     *
     * @param string $typeBillet
     *
     * @return Commande
     */
    public function setTypeBillet($typeBillet)
    {
        $this->typeBillet = $typeBillet;

        return $this;
    }

    /**
     * Get typeBillet
     *
     * @return string
     */
    public function getTypeBillet()
    {
        return $this->typeBillet;
    }

}
