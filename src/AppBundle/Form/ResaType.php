<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateVisite',DateType::class, array(
                'label' => 'Date de Visite',
                'widget' => 'single_text',
                'html5' => false,
                'required' => 'true',
                'input' => 'datetime',
                'placeholder' => 'informations.calendrier',
                'format' => 'dd/MM/yyyy'))
            ->add('nbBillets')
            ->add('typeBillet', ChoiceType::class, array(
                'choices' => array(
                    'Journée' => 'true', 'Demi-Journée' => 'false'),
                'choice_translation_domain' => 'messages'
            ))
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, array('label' => 'bouton.valider', 'attr' => ['class' => 'center-block btn btn-success']))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Commande'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_commande';
    }


}
