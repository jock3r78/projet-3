<?php

namespace AppBundle\Validator;

use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;

class RestrictedDatesValidator extends ConstraintValidator

{
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint)
    {

        $currentDate = new \DateTime("now", new \DateTimeZone('Europe/Paris'));

        // On empêche de réserver sans date
        if (empty($value)) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
            return false;
        }
        $day = $value->format('l');
        $year = intval(date('Y'));

        $easterDate = easter_date($year);
        $easterDay = date('j', $easterDate);
        $easterMonth = date('n', $easterDate);
        $easterYear = date('Y', $easterDate);
        $holidays = array(
            // Dates fixes
            date('d-m', (mktime(0, 0, 0, 1, 1, $year))),  // 1er janvier
            date('d-m', (mktime(0, 0, 0, 5, 1, $year))),  // Fête du travail
            date('d-m', (mktime(0, 0, 0, 5, 8, $year))),  // Victoire des alliés
            date('d-m', (mktime(0, 0, 0, 7, 14, $year))),  // Fête nationale
            date('d-m', (mktime(0, 0, 0, 8, 15, $year))),  // Assomption
            date('d-m', (mktime(0, 0, 0, 11, 1, $year))),  // Toussaint
            date('d-m', (mktime(0, 0, 0, 11, 11, $year))),  // Armistice
            date('d-m', (mktime(0, 0, 0, 12, 25, $year))),  // Noel
            // Dates variables
            date('d-m', mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear)),
            date('d-m', mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear)),
            date('d-m', mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear)),
        );

        // Si on est un mardi, dimanche ou un jour férié, on bloque la réservation
        if ($day === 'Tuesday' || $day === 'Sunday' || $day === $holidays) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }

        // On bloque les commandes sur la journée en cours après 18:00
        if ($value->format('Ymd') == $currentDate->format('Ymd') && $currentDate->format('H') >= 18) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
            return false;
        }



    }
}

