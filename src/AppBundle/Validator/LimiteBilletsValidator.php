<?php


namespace AppBundle\Validator;


use AppBundle\Entity\Commande;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LimiteBilletsValidator extends ConstraintValidator

{
    private $em;
    private $stock;

    public function __construct(EntityManager $em, $stock)
    {
        $this->em = $em;
        $this->stock=$stock;
    }

    public function validate($value, Constraint $constraint)
    {
        $commande = $this->context->getRoot()->getData();

        $date = $commande->getDateVisite($value);
        $nbBilletsComm = $commande->getNbBillets();

        $commandes = $this->em->getRepository('AppBundle:Commande')->findBy(array(
            'dateVisite' => $date,
            'paymentStatus' => Commande::STATUS_PAID
        ));
        $total = 0;
        foreach ($commandes as $commande) {
            $total += $commande->getNbBillets();
        }

        $totalRestant =  $this->stock - $total;

        if (($total + $nbBilletsComm) >= $this->stock ) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('%totalRestant%', $totalRestant)
                ->addViolation();
        }
    }
}