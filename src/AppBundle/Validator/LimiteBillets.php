<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

class LimiteBillets extends Constraint
{

    public $message = "Le quota de billets pour ce jour de visite a été dépassé (%totalRestant% billet(s) restant(s))";

    public function validatedBy()
    {
        return 'limitebillets.validator'; // Ici, on fait appel à l'alias du service
    }
}