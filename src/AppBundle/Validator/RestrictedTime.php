<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 21/01/2017
 * Time: 16:00
 */

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

class RestrictedTime extends Constraint
{

    public $message = "Vous ne pouvez réserver de billet journée après 14H";


}