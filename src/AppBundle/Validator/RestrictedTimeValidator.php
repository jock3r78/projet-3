<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 21/01/2017
 * Time: 16:00
 */
namespace AppBundle\Validator;

use AppBundle\Entity\Commande;
use AppBundle\Entity\Ticket;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use DateTime;

class RestrictedTimeValidator extends ConstraintValidator

{


    public function validate($value, Constraint $constraint)
    {

        $currentDate = new \DateTime("now", new \DateTimeZone('Europe/Paris'));
        $dateToday = $currentDate->format('H:i');
        $commande = $this->context->getRoot()->getData();
        if ($value === 'true' && $dateToday > '14:00' && $commande->getDateVisite()->format('dmY') == $currentDate->format('dmY')) {
            $this->context->addViolation($constraint->message);

        }
    }
}