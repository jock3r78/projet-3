<?php


namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */

class RestrictedDates extends Constraint
{

    public $message = "Vous ne pouvez réserver de billet à cette date (Musée Fermé)";

    public function validatedBy()
    {
        return 'restricteddates.validator'; // Ici, on fait appel à l'alias du service
    }
}