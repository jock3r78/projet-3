<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Commande;

class LoadCommandes implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nbBillets = array(900, 90, 2, 5); // On load 997 sur 4 commandes à la date du jour comme date de
        // visite et de reservation (constructeur)
        $type_billet = true;

        foreach ($nbBillets as $nbBillet) {
            $commande = new Commande();
            $commande->setNbBillets($nbBillet);
            $commande->setTypeBillet($type_billet);
            $commande->setPaymentStatus(Commande::STATUS_PAID);
            $manager->persist($commande);
        }
        $manager->flush();

    }
}
